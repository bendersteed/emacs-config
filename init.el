;; fast init confs

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;; package src

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives
	     '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
	     '("org" . "https://orgmode.org/elpa/") t)
(add-to-list 'load-path "~/.emacs.d/lisp/")
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e/")
(add-to-list 'load-path "~/src/ox-haunt/")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(modus-operandi))
 '(custom-safe-themes
   '("d2db4af7153c5d44cb7a67318891e2692b8bf5ddd70f47ee7a1b2d03ad25fcd9" "104a89bfa7a5cdf91a81232f2b45180b7c8a0b70f13a1d3851237563bf393784" "752d4dc1e784d2e31c57e7fdf769c916c74cab59e27c08d1e5c4b683df59bb94" "cfa3b266957e26ed5a8637f43d443b4a921bb546381d7df97e7338d278184fa9" "7b3ce93a17ce4fc6389bba8ecb9fee9a1e4e01027a5f3532cc47d160fe303d5a" "c74807973af861d4997320ca7fce9238bb132d4b12a29cf4cbb59de26c5abfbd" "6a9e48e3ff2170edd584d2e9655d3b5c44a8d5a36ac5e52e9db98671a606e345" "8e709853a69760dc9ae57ff0bd3aa3e6f21e8315acc381330ca676e4bd9d1c92" "b2ca1d7e8f433fd1bab6bf3ac5ad61ee6398c3cff152fd0c499af94aadb964b1" "9fe1200e682dec8d27d8bf8b0e1b3398a9d39ec1d2e66409217df7fc6ed9faf5" "2e942be1f563eb7ecf867a44b35752a04cd3514acad5e25f4dce119e53c13556" "b7e66d62669194652257afbec5baa9909b261aa219022aa4e5b0319c9955bc01" "9fa66f79bafed8567ee74109bab390a529b639881b86b69f1c64bcdec00e80ad" "00664002472a541e3df8a699c2ea4a5474ea30518b6f9711fdf5fe3fe8d6d34f" "c771affc8912ccee6fc82793c31f7537063600f42b24ac8fceb55a2624035571" "5d9f039867c96effeae8214fd0a225344bfb6cb6bb43b999d86f90b97a8a2ae2" "f666f384eed5148982150975ab26f7bcefbfb668806f53c2f460bad8e3115662" "77c74e2a34d16a0d531686f7a1e3d864346daa18624d78ea2defd4bb226234e4" "1e349b85ab16972dd83ad72292762b468d93fcb875bf916b8629e1e84e0144ec" "6fa44c1837d5242db12386b05d44a8220b144795bed28294ee7cc725b93e0ceb" "31d440c71145f6a24aaee6ec297560b46729402dac67fd4abeccab77dc967175" "5310a2d34612a68db8557433fe9fd3d2af1e6a3b284b4e77ce09858ccfd8c1b7" "3dbb18bf06f41012d4525e6c64c392d6cfef06a2f8fe1bf7b565c4e020255466" "3b116632d401aa4a6fce54b82d746b5441f543fa96619c1c79923e2f68ce1665" "d447f60b0e99970b848fcd38a922c7b1f5202585bf4c7c006b8e90d6ed6dd146" "9196767d9aed967c1621fe43372860d9b219dbca4b0c6adb07603499c18fa79b" "b7638495c88ed9ef3a42eac95a51a54429c12981ebdd915a8ccd068089e2cecc" "b4f69ef46dd3da81b157325c0ea4fafdbd91ad5a8f1d2fa4b11590acc40c70c8" "17cf2bd8e2f618ce575d9f352a843e5656a497c12ec5771b2ad52c56ccd088d8" "cc63dade39a93f82519c9eef9cbf4ba7d4378a81f46250ccfa5a29119b96e253" "113a607d8d3cce4ee00a6f1f726cb635628e98c27b2f092bdb498b7bc4427348" "48bde6c9733775b7c27fb82bb3ce78961e56a429c68845ebb74866fd8d1f90fa" "9a2bae2d03f8d9cfd9211ff7709aefcb269a4f076921aaef531b79fc6ae23792" "87fd15a92096797894626d25d8f8a436b90ce8d97d499a98faea972944645fbd" "546cfae0b2944459a09e57c241297703b188d69530508b8e74d93fa251f68370" "400c1680bec5553c778aa8ae38ede204b4d453921c0f379c9481b2ff25e85bda" "f693e6a6069f9dbb06f9f97d08f9900dbb76f1d68c2c0273eac085c64dbe8dcd" "3baf60d1cf17d68062f0312ae4587ac87f975674751fc2feb1dfda354310acac" "ca35cfe9f901e1058b31099b1e5c84d2fa1b9f16eeb7f838cfbd89d74ed960dc" "e5693ff27e0480b979cee580ea27e36b4848d5903717ac906bb9478d0e3f61d8" "e129ee166c2cd586fb0831c711fc49977a065360461ba9ac78786be822ab4338" "c0350aed6dc98abdc329906a630b4cdf8ebb147cdf2a873e2648dfc0b904b2ab" "befd48e22121985f7bdbb188704bd3fdb12697d5031c0f0c4d4cb34f8fa3024c" "e9f971278e22486e12d5ebed05ba89e2f7ae315e6202b93af530aad8121660d9" "801a567c87755fe65d0484cb2bded31a4c5bb24fd1fe0ed11e6c02254017acb2" "e91d8141271c42bf71ceb6c817b677b8df88689f7c7325fcfff8a283a151188e" "a9c619535d63719a15f22e3c450a03062d3fed1e356ef96d33015849c4c43946" "ecba61c2239fbef776a72b65295b88e5534e458dfe3e6d7d9f9cb353448a569e" "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36" "f0dc4ddca147f3c7b1c7397141b888562a48d9888f1595d69572db73be99a024" "7e78a1030293619094ea6ae80a7579a562068087080e01c2b8b503b27900165c" default))
 '(default-input-method "greek")
 '(exec-path
   '("/usr/local/bin" "/usr/bin" "/usr/local/libexec/emacs/27.1/x86_64-pc-linux-gnu" "/home/bendersteed/bin" "/home/bendersteed/.local/bin"))
 '(org-agenda-files
   '("/home/bendersteed/notes/projects/webring-link-functionality---haunt.org" "/home/bendersteed/notes/blog.org" "/home/bendersteed/notes/ideas.org" "/home/bendersteed/notes/inbox.org" "/home/bendersteed/notes/lore.org" "/home/bendersteed/notes/media.org" "/home/bendersteed/notes/personal.org" "/home/bendersteed/notes/poems.org" "/home/bendersteed/notes/tasks.org" "/home/bendersteed/notes/vampire.org" "/home/bendersteed/notes/wr.org" "/home/bendersteed/notes/projects/astronomy-coursera.org" "/home/bendersteed/notes/projects/breaktheborders-gr-posts-el.org" "/home/bendersteed/notes/projects/breaktheborders-gr-posts-en.org" "/home/bendersteed/notes/projects/breaktheborders.org" "/home/bendersteed/notes/projects/brsd.org" "/home/bendersteed/notes/projects/coursera:-algorithms-i.org" "/home/bendersteed/notes/projects/discordia-chan.org" "/home/bendersteed/notes/projects/guixsd.org" "/home/bendersteed/notes/projects/math.org" "/home/bendersteed/notes/projects/olga-website.org" "/home/bendersteed/notes/projects/podcast.org" "/home/bendersteed/notes/projects/quantum-mechanics-coursera.org" "/home/bendersteed/notes/projects/snafu.org" "/home/bendersteed/notes/projects/taratsa.org" "/home/bendersteed/notes/projects/teach-yourself-cs.org" "/home/bendersteed/notes/projects/thouria-votana.org" "/home/bendersteed/notes/projects/xelwna-radio-station.org" "/home/bendersteed/notes/work/ampersand.org" "/home/bendersteed/notes/work/guix-presentation.org" "/home/bendersteed/notes/work/lessons.org"))
 '(org-format-latex-options
   '(:foreground default :background default :scale 1.3 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
                 ("begin" "$1" "$" "$$" "\\(" "\\[")))
 '(org-latex-packages-alist '(("" "physics" t)))
 '(package-selected-packages
   '(all-the-icons org-drill common-lisp-snippets yasnippet-snippets embark orderless sx org-plus-contrib modus-themes modus-theme google-translate ess sicp haskell-mode elpher ggtags lua-mode pomodoro gnuplot nov diff-hl elfeed org-bullets ox-reveal htmlize wgrep web-mode use-package slime rainbow-delimiters projectile pdf-tools password-store paredit org-noter olivetti magit iedit graphviz-dot-mode geiser emojify emmet-mode dired-rsync cider auctex ag add-node-modules-path))
 '(safe-local-variable-values
   '((projectile-project-install-cmd . "rsync -Pr src/wikisophy/fe/public/ user@bendersteed.tech:/var/www/wikisophy.bendersteed.tech/public_html/")
     (projectile-project-install-cmd . "rsync -Pr src/xelwna/site/ user@snf-872448.vm.okeanos.grnet.gr:/var/www/xelwna.space/public_html/")
     (geiser-guile-load-path . "/home/bendersteed/src/bendersteed-tech")
     (eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; general interface tweaks
(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 800000))

(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)
(setq inhibit-startup-message t)
(setq inhibit-compacting-font-caches t)
(setq bidi-inhibit-bpa t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(column-number-mode 1)
(setq frame-resize-pixelwise t)         ; for better resizing in tiling scenarios
(setq read-process-output-max (* 1024 1024))
(setq visible-bell nil
      ring-bell-function 'flash-mode-line)
(defun flash-mode-line ()
  (invert-face 'mode-line)
  (run-with-timer 0.1 nil #'invert-face 'mode-line))
(setq-default fill-column 100)
(define-key minibuffer-local-map (kbd "C-o") 'switch-to-completions)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-x k") 'kill-current-buffer)
(global-set-key (kbd "C-*") 'calc)
(set-face-attribute 'variable-pitch nil :family "Ubuntu"
                    :height 120)
(set-face-attribute 'fixed-pitch nil :family "Ubuntu Mono"
                    :height 120)
(add-to-list 'default-frame-alist
             '(font . "Ubuntu Mono"))
(fset 'yes-or-no-p 'y-or-n-p)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c m") 'morning-routine)
(global-set-key (kbd "C-c d") 'diff-buffer-with-file)
(setq tab-always-indent 'complete)
(show-paren-mode t)
(electric-pair-mode 1)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-c C-;") 'eshell)
(setq load-prefer-newer t)
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-file-name-partially
        try-complete-file-name
        try-expand-all-abbrevs
        try-expand-list
        try-expand-line
        try-complete-lisp-symbol-partially
        try-complete-lisp-symbol))
(windmove-default-keybindings)
(setq-default frame-title-format '("%b [%m]"))
(setq mouse-drag-and-drop-region 1)
(setq-default indent-tabs-mode nil)
(setq mml-secure-smime-sign-with-sender t)
(setq mml-secure-openpgp-sign-with-sender t)

;; ibuffer configuration
(use-package ibuffer
  :config
  (setq ibuffer-expert t)
  (setq ibuffer-display-summary nil)
  (setq ibuffer-use-other-window nil)
  (setq ibuffer-show-empty-filter-groups nil)
  (setq ibuffer-movement-cycle nil)
  (setq ibuffer-default-sorting-mode 'filename/process)
  (setq ibuffer-use-header-line t)
  (setq ibuffer-default-shrink-to-minimum-size nil)
  (setq ibuffer-formats
        '((mark modified read-only locked " "
                (name 30 30 :left :elide)
                " "
                (size 9 -1 :right)
                " "
                (mode 16 16 :left :elide)
                " " filename-and-process)
          (mark " "
                (name 16 -1)
                " " filename)))
  (setq ibuffer-saved-filter-groups nil)
  :hook
  (ibuffer-mode . hl-line-mode)
  :bind (("C-x C-b" . ibuffer)
         ("s-b" . switch-to-buffer)
         :map ibuffer-mode-map
         ("* f" . ibuffer-mark-by-file-name-regexp)
         ("* g" . ibuffer-mark-by-content-regexp) ; "g" is for "grep"
         ("* n" . ibuffer-mark-by-name-regexp)
         ("s n" . ibuffer-do-sort-by-alphabetic)  ; "sort name" mnemonic
         ("/ g" . ibuffer-filter-by-content)))

;;; windows
;;; by protesilaos
(use-package window
  :init
  (setq display-buffer-alist
        '(;; top side window
          ("\\**prot-elfeed-bongo-queue.*"
           (display-buffer-reuse-window display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . -2))
          ("\\*\\(prot-elfeed-mpv-output\\|world-clock\\).*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . -1))
          ("\\*\\(Flymake\\|Package-Lint\\|vc-git :\\).*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . 0)
           (window-parameters . ((no-other-window . t))))
          ("\\*Messages.*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . 1)
           (window-parameters . ((no-other-window . t))))
          ("\\*\\(Backtrace\\|Warnings\\|Compile-Log\\)\\*"
           (display-buffer-in-side-window)
           (window-height . 0.16)
           (side . top)
           (slot . 2)
           (window-parameters . ((no-other-window . t))))
          ;; bottom side window
          ("\\*\\(Embark\\)?.*Completions.*"
           (display-buffer-in-side-window)
           (side . bottom)
           (slot . 0)
           (window-parameters . ((no-other-window . t)
                                 (mode-line-format . none))))
          ;; left side window
          ("\\*Help.*\\|\\*Racket.*"
           (display-buffer-in-side-window)
           (window-width . 0.30)        ; See the :hook
           (side . left)
           (slot . 0)
           (window-parameters . ((no-other-window . t))))
          ;; right side window
          ("\\*Faces\\*"
           (display-buffer-in-side-window)
           (window-width . 0.25)
           (side . right)
           (slot . 0)
           (window-parameters
            . ((mode-line-format
                . (" "
                   mode-line-buffer-identification)))))
          ("\\*Custom.*"
           (display-buffer-in-side-window)
           (window-width . 0.25)
           (side . right)
           (slot . 1)
           (window-parameters . ((no-other-window . t))))
          ;; bottom buffer (NOT side window)
          ("\\*\\vc-\\(incoming\\|outgoing\\).*"
           (display-buffer-at-bottom))
          ("\\*\\(Output\\|Register Preview\\).*"
           (display-buffer-at-bottom)
           (window-parameters . ((no-other-window . t))))
          ("\\*.*\\([^E]eshell\\|shell\\|v?term\\).*"
           (display-buffer-reuse-mode-window display-buffer-at-bottom)
           (window-height . 0.2)
           ;; (mode . '(eshell-mode shell-mode))
           )))
  (setq window-combination-resize t)
  (setq even-window-sizes 'height-only)
  (setq window-sides-vertical nil)
  (setq switch-to-buffer-in-dedicated-window 'pop)
  (setq mouse-autoselect-window t
        focus-follows-mouse t)
  (defun switch-to-completions-or-other ()
    "Jump to other window or *Completions* buffer if one is active. "
    (interactive)
    (if (and (get-buffer-window "*Completions*" 0)
             (not (string-equal (buffer-name (current-buffer)) "*Completions*")))
        (switch-to-completions)
      (other-window 1)))
  ;; Hooks' syntax is controlled by the `use-package-hook-name-suffix'
  ;; variable.  The "-hook" suffix is intentional.
  :hook ((help-mode-hook . visual-line-mode)
         (custom-mode-hook . visual-line-mode))
  :bind (("s-n" . next-buffer)
         ("s-N" . previous-buffer)
         ("C-o" . switch-to-completions-or-other)
         ("C-2" . split-window-below)
         ("C-3" . split-window-right)
         ("C-0" . delete-window)
         ("C-1" . delete-other-windows)
         ("C-5" . delete-frame)
         ("C-x _" . balance-windows)
         ("C-x +" . balance-windows-area)
         ("M-s-q" . window-toggle-side-windows)))

(use-package ace-window
  :ensure t
  :bind (("M-o" . ace-window)))

;; so-long mode
(use-package so-long
  :defer 4
  :config
  (global-so-long-mode t))

;; minor modes and utils
(use-package goto-addr
  :hook ((compilation-mode . goto-address-mode)
         (prog-mode . goto-address-mode)
         (eshell-mode . goto-address-mode)
         (shell-mode . goto-address-mode))
  :bind (:map goto-address-highlight-keymap
              ("<RET>" . goto-address-at-point)
              ("M-<RET>" . newline))
  :commands (goto-address-prog-mode
             goto-address-mode))

(use-package hideshow
  :hook (prog-mode . hs-minor-mode)
  :config
  ;; Add `json-mode' and `javascript-mode' to the list
  (setq hs-special-modes-alist
        (mapcar 'purecopy
                '((c-mode "{" "}" "/[*/]" nil nil)
                  (c++-mode "{" "}" "/[*/]" nil nil)
                  (java-mode "{" "}" "/[*/]" nil nil)
                  (js-mode "{" "}" "/[*/]" nil)
                  (json-mode "{" "}" "/[*/]" nil)
                  (javascript-mode  "{" "}" "/[*/]" nil)))))

(load "my-flash.el")

(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package subword
  :hook (prog-mode . subword-mode))

(use-package flymake
  :hook (prog-mode . flymake-mode)
  :config
  (setq flymake-start-syntax-check-on-newline nil)
  (remove-hook 'flymake-diagnostic-functions 'flymake-proc-legacy-flymake)
  (define-key flymake-mode-map (kbd "M-n") 'flymake-goto-next-error)
  (define-key flymake-mode-map (kbd "M-p") 'flymake-goto-prev-error))

(use-package iedit
  :ensure t
  :after prog-mode)

(use-package yasnippet
  :ensure t
  :hook ((org-mode . yas-minor-mode)
         (prog-mode . yas-minor-mode))
  :config
  (yas-reload-all)
  (setq yas-snippet-dirs '("~/.emacs.d/snippets")))

;; dired
(use-package dired
  :config
  (setq save-interprogram-paste-before-kill t
	dired-dwim-target t)
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  (setq dired-listing-switches "-AFhlv --group-directories-first")
  (setq dired-dwim-target t)
  :hook ((dired-mode . dired-hide-details-mode)
	 (dired-mode . hl-line-mode)))

(use-package dired-rsync
  :ensure t
  :bind (:map dired-mode-map
              ("C-c C-r" . dired-rsync)))

(use-package gnus-dired
  :after (gnus dired)
  :hook (dired-mode . gnus-dired-mode))

;; magit and vc
(use-package magit
  :bind ("C-x g" . magit-status))

(use-package diff-hl
  :after vc
  :config
  (setq diff-hl-draw-borders nil)
  (setq diff-hl-side 'left)
  (diff-hl-flydiff-mode)
  :hook ((prog-mode . diff-hl-mode)
	 (dired-mode . diff-hl-dired-mode)))

;; which key
(use-package orderless
  :ensure t
  :config (setq orderless-component-separator "[ &]"))

(use-package minibuffer
  :after orderless
  :config
  (setq completion-styles '(partial-completion substring flex orderless))
  (setq completion-cycle-threshold 3)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-pcm-word-delimiters "-_./:| ")
  (setq completion-show-help nil)
  (setq completion-auto-help t)
  (setq completion-ignore-case t)
  (setq-default case-fold-search t)   ; For general regexp

  (setq completions-detailed t)

  (setq read-buffer-completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)

  (setq enable-recursive-minibuffers t)
  (setq read-answer-short t)
  (setq resize-mini-windows t)
  (setq minibuffer-eldef-shorten-default t)

  (file-name-shadow-mode 1)
  (minibuffer-depth-indicate-mode 1)
  (minibuffer-electric-default-mode 1)
 
  :bind (("s-b" . switch-to-buffer)
         ("s-B" . switch-to-buffer-other-window)
         ("s-f" . find-file)
         ("s-F" . find-file-other-window)
         :map minibuffer-local-completion-map
         ("C-j" . exit-minibuffer)
         ("<tab>" . minibuffer-force-complete)
         :map completion-list-mode-map
         ("n" . next-line)
         ("p" . previous-line)
         ("f" . next-completion)
         ("b" . previous-completion)))

(use-package embark
  :config
  (setq embark-quit-after-action t)
  (setq embark-action-indicator
        (lambda (map)
          (which-key--show-keymap "Embark" map nil nil 'no-paging)
          #'which-key--hide-popup-ignore-command)
        embark-become-indicator embark-action-indicator)
  :bind ("C-," . embark-act))

(use-package isearch
  :diminish
  :config
  (setq search-highlight t)
  (setq search-whitespace-regexp ".*?")
  (setq isearch-lax-whitespace t)
  (setq isearch-regexp-lax-whitespace nil)
  (setq isearch-lazy-highlight t)
  (setq isearch-lazy-count t)
  (setq isearch-yank-on-move 'shift)
  (setq isearch-allow-scroll 'unlimited)
  :bind (:map minibuffer-local-isearch-map
         ("M-/" . isearch-complete-edit)
         :map isearch-mode-map
         ("C-g" . isearch-cancel)       ; instead of `isearch-abort'
         ("M-/" . isearch-complete)))

(use-package replace
  :config
  (setq list-matching-lines-jump-to-current-line t)
  :hook ((occur-mode-hook . hl-line-mode)
         (occur-mode-hook . (lambda ()
                              (toggle-truncate-lines t)))))

(use-package autorevert
  :config
  (setq auto-revert-verbose t)
  :hook (after-init-hook . global-auto-revert-mode))

(use-package wgrep
  :defer t)

;; auth
(use-package password-store
  :ensure t
  :config
  (setq password-store-password-length 16))

(use-package auth-source
  :config
  (auth-source-pass-enable))

;; themes and modeline
(use-package modus-themes
  :ensure t
  :config
  (setq modus-themes-slanted-constructs t
        modus-themes-bold-constructs t
        modus-themes-visible-fringes t
        modus-themes-distinct-org-blocks t
        modus-themes-rainbow-headings t
        modus-themes-proportional-fonts nil
        modus-themes-scale-headings nil
        modus-themes-scale-1 1.05
        modus-themes-scale-2 1.1
        modus-themes-scale-3 1.15
        modus-themes-scale-4 1.2))
        
(use-package all-the-icons
  :defer t
  :ensure t)

(use-package emojify
  :ensure t
  :commands emojify-mode)

(use-package proced
  :commands proced
  :config
  (setq proced-auto-update-flag t)
  (setq proced-auto-update-interval 1)
  (setq proced-descend t)
  (setq proced-filter 'user))

(use-package shr
  :commands (eww
             eww-browse-url)
  :config
  (setq browse-url-browser-function 'eww-browse-url)
  (setq shr-use-fonts nil)
  (setq shr-use-colors t)
  (setq shr-max-image-proportion 1)
  (setq shr-width (current-fill-column)))

(use-package elpher
  :ensure t
  :hook (elpher-mode . olivetti-mode))

(use-package eww
  :commands eww
  :hook (eww-mode . olivetti-mode)
  :config
  (setq url-cookie-trusted-urls '()
        url-cookie-untrusted-urls '(".*"))
  
  (defun my-eww-browse-url (original url &optional new-window)
  "Handle gemini links."
  (cond ((string-match-p "\\`\\(gemini\\|gopher\\)://" url)
	 (require 'elpher)
	 (elpher-go url))
	(t (funcall original url new-window))))

  (advice-add 'eww-browse-url :around 'my-eww-browse-url))

;; pdf-tools
(use-package pdf-tools
  :ensure t
  :mode ("\\.pdf\\'" . pdf-view-mode)
  :config
  ;; automatically annotate highlights
  (setq pdf-annot-activate-created-annotations t)
  ;; more fine-grained zooming
  (setq pdf-view-resize-factor 1.1)
  ;; keyboard shortcuts
  (define-key pdf-view-mode-map (kbd "h") 'pdf-annot-add-highlight-markup-annotation)
  (define-key pdf-view-mode-map (kbd "t") 'pdf-annot-add-text-annotation)
  (define-key pdf-view-mode-map (kbd "D") 'pdf-annot-delete))

(use-package nov
  :ensure t
  :mode ("\\.epub\\'" . nov-mode))

;; paredit
(use-package paredit
  :ensure t
  :hook (prog-mode . paredit-mode))

;; projectile
(use-package projectile
  :ensure t
  :hook (after-init . projectile-mode)
  :bind-keymap ("C-c p" . projectile-command-map)
  :config
  (setq projectile-indexing-method 'alien)
  (setq projectile-project-search-path '("~/src/"))
  (projectile-register-project-type 'haunt '("haunt.scm")
                                    :compile "haunt build"
		                    :run "haunt serve"))

(use-package ag
  :ensure t
  :after projectile)

;; olivetti
(use-package olivetti
  :commands olivetti-mode
  :ensure t
  :config
  (setq olivetti-body-width 100)
  (setq olivetti-minimum-body-width 100)
  (setq olivetti-recall-visual-line-mode-entry-state t))

;;; google translate
(use-package google-translate
  :ensure t
  :bind (:map global-map
              ("C-c M-t" . google-translate-smooth-translate))
  :custom
  (google-translate-backend-method 'curl)
  :config
  (defun google-translate--search-tkk ()
    "Search TKK."
    (list 430675 2721866130)))

;; prog modes
(use-package geiser
  :commands geiser
  :ensure t)

(use-package racket-mode
  :ensure t
  :mode "\\.rkt\\'"
  :hook (racket-mode . racket-xp-mode))

(use-package cider
  :commands cider
  :ensure t)

(use-package web-mode
  :ensure t
  :mode
  (("\\.html\\'" . web-mode)
   ("\\.phtml\\'" . web-mode)
   ("\\.tpl\\.php\\'" . web-mode)
   ("\\.[agj]sp\\'" . web-mode)
   ("\\.as[cp]x\\'" . web-mode)
   ("\\.erb\\'" . web-mode)
   ("\\.mustache\\'" . web-mode)
   ("\\.djhtml\\'" . web-mode)
   ("\\.vue\\'" . web-mode))
  :config
  (setq web-mode-enable-auto-pairing t)
  (setq web-mode-enable-css-colorization t))

(use-package emmet-mode
  :ensure t
  :hook (web-mode . emmet-mode))

(use-package slime
  :ensure t
  :commands slime
  :config
  (setq inferior-lisp-program "/usr/bin/sbcl")
  (setq common-lisp-hyperspec-root
	(concat "file://" (expand-file-name "~/src/hyper-spec/HyperSpec/")))
  (slime-setup '(inferior-slime slime-autodoc slime-banner slime-xref-browser
				slime-references slime-presentations
				slime-presentation-streams slime-fancy-inspector
				slime-trace-dialog
				slime-indentation)))

(use-package graphviz-dot-mode
  :ensure t
  :mode (("\\.dot\\'" . graphviz-dot-mode)
         ("\\.gv\\'" . graphviz-dot-mode)))

;; octave
(use-package octave
  :mode ("\\.m$" . octave-mode))

;; python
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "--simple-prompt -i")
(setq org-babel-python-command "python3")

;; javascript
(use-package add-node-modules-path
  :ensure t
  :after js-mode
  :hook ((js-mode web-mode) add-node-modules-path))

;; lsp
(use-package eglot
  :ensure t
  :commands eglot)

;; r for statistics
(use-package ess
  :ensure t
  :mode ("\\.r$" . ess-mode))

;; auctex
(use-package tex-site
  :mode ("\\.tex\\'" . TeX-latex-mode))

(use-package latex
  :defer t
  :config
  ;; Use pdf-tools to open PDF files
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
	TeX-source-correlate-start-server t)
  ;; Update PDF buffers after successful LaTeX runs
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)
  (setq-default TeX-PDF-mode t))

(use-package hydra
  :ensure t
  :init
  (global-set-key
   (kbd "C-c t")
   (defhydra toggle (:color blue)
     "toggle"
     ("s" ispell "ispell")
     ("d" toggle-debug-on-error "debug")
     ("c" flymake-mode "flymake")
     ("f" auto-fill-mode "fill")
     ("m" org-math-modes "org-math")
     ("o" olivetti-mode "olivetti")
     ("p" paredit-mode "paredit")
     ("t" toggle-truncate-lines "truncate")
     ("w" whitespace-mode "whitespace")
     ("q" nil "cancel")))
  (global-set-key
   (kbd "C-c j")
   (defhydra gotoline
     ( :pre (linum-mode 1)
       :post (linum-mode -1))
     "goto"
     ("t" (lambda () (interactive)(move-to-window-line-top-bottom 0)) "top")
     ("b" (lambda () (interactive)(move-to-window-line-top-bottom -1)) "bottom")
     ("m" (lambda () (interactive)(move-to-window-line-top-bottom)) "middle")
     ("e" (lambda () (interactive)(end-of-buffer)) "end")
     ("c" recenter-top-bottom "recenter")       ("c" recenter-top-bottom "recenter")
     ("n" next-line "down")
     ("p" (lambda () (interactive) (forward-line -1))  "up")
     ("g" goto-line "goto-line")
     ("q" nil "cancel")))
  (global-set-key
   (kbd "C-c o")
   (defhydra hydra-global-org (:color blue)
     "Org"
     ("t" org-timer-start "Start Timer")
     ("s" org-timer-stop "Stop Timer")
     ("r" org-timer-set-timer "Set Timer") ; This one requires you be in an orgmode doc, as it sets the timer for the header
     ("p" org-timer "Print Timer")	; output timer value to buffer
     ("i" org-clock-in "Clock-In") ; used with (org-clock-persistence-insinuate) (setq org-clock-persist t)
     ("o" org-clock-out "Clock-Out") ; you might also want (setq org-log-note-clock-out t)
     ("j" org-clock-goto "Clock Goto") ; global visit the clocked task
     ("q" nil "cancel"))))

;; Outgoing email (msmtp + msmtpq) + incoming mail (mu)
(use-package smtpmail
  :defer 4
  :config
  (setq send-mail-function 'sendmail-send-it
        sendmail-program "/usr/bin/msmtp"
        user-mail-address "me@bendersteed.tech"
        user-full-name "Dimos Dimakakos"))

(use-package mu4e
  :commands mu4e
  :config
  (setq mu4e-maildir "~/mail"
        mu4e-sent-folder "/riseup/Sent"
        mu4e-refile-folder "/riseup/Archive")
  (setq mu4e-get-mail-command "mbsync -Va")
  (setq mu4e-change-filenames-when-moving t)
  (setq mu4e-view-use-gnus t)
  :hook (mu4e-headers-mode . hl-line-mode))

;; gnus for mailing lists
(use-package gnus
  :commands gnus
  :config
  ;; accounts
  (setq gnus-select-method '(nntp "news.gwene.org"))
  (setq gnus-gcc-mark-as-read t)
  (setq gnus-novice-user nil)
  ;; checking sources
  (setq gnus-check-new-newsgroups 'ask-server)
  (setq gnus-read-active-file 'some)
  ;; dribble
  (setq gnus-use-dribble-file t)
  (setq gnus-always-read-dribble-file t))

(use-package gnus-group
  :after gnus
  :demand
  :config
  (setq gnus-level-subscribed 6)
  (setq gnus-level-unsubscribed 7)
  (setq gnus-level-zombie 8)
  (setq gnus-list-groups-with-ticked-articles nil)
  (setq gnus-group-sort-function
        '((gnus-group-sort-by-unread)
          (gnus-group-sort-by-alphabet)
          (gnus-group-sort-by-rank)))
  (setq gnus-group-mode-line-format "%%b")
  :hook
  (gnus-select-group-hook . gnus-group-set-timestamp)
  (gnus-group-mode . hl-line-mode))

(use-package gnus-topic
  :after (gnus gnus-group)
  :config
  (setq gnus-topic-display-empty-topics nil)
  :hook
  (gnus-group-mode . gnus-topic-mode))

(use-package gnus-sum
  :after (gnus gnus-group)
  :demand
  :config
  (setq gnus-auto-select-first nil)
  (setq gnus-summary-ignore-duplicates t)
  (setq gnus-suppress-duplicates t)
  (setq gnus-summary-goto-unread nil)
  (setq gnus-summary-make-false-root 'adopt)
  (setq gnus-summary-thread-gathering-function
        'gnus-gather-threads-by-subject)
  (setq gnus-thread-sort-functions
        '((not gnus-thread-sort-by-date)
          (not gnus-thread-sort-by-number)))
  (setq gnus-subthread-sort-functions
        'gnus-thread-sort-by-date)
  (setq gnus-thread-hide-subtree nil)
  (setq gnus-thread-ignore-subject nil)
  (setq gnus-user-date-format-alist
        '(((gnus-seconds-today) . "Today at %R")
          ((+ 86400 (gnus-seconds-today)) . "Yesterday, %R")
          (t . "%Y-%m-%d %R")))
  (setq gnus-summary-line-format "%U%R%z %-16,16&user-date;  %4L:%-30,30f  %B%S\n")
  (setq gnus-summary-mode-line-format "%p")
  (setq gnus-sum-thread-tree-false-root "─┬➤ ")
  (setq gnus-sum-thread-tree-indent " ")
  (setq gnus-sum-thread-tree-leaf-with-other "├─➤ ")
  (setq gnus-sum-thread-tree-root "")
  (setq gnus-sum-thread-tree-single-leaf "└─➤ ")
  (setq gnus-sum-thread-tree-vertical "│")
  :hook
  (gnus-summary-mode . hl-line-mode)
  (gnus-summary-exit-hook . gnus-topic-sort-groups-by-alphabet)
  (gnus-summary-exit-hook . gnus-group-sort-groups-by-rank))

;; org-mode
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :config
  (setq org-fast-tag-selection-include-todo t)
  (setq org-log-done 'time)
  (setq org-modules
        '(org-habit))
  
  (load "my-org.el")
  ;; custom state
  (setq org-use-fast-todo-selection t)
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d/!)")
          (sequence "WAITING(w@/!)" "INACTIVE(i@/!)" "|" "CANCELLED(c@/!)" "MEETING")))

  (global-set-key "\C-cl" 'org-store-link)

  (setq org-clock-in-switch-to-state "NEXT")
  (setq org-clock-out-switch-to-state "TODO")
  ;; capture templates and blog posts
  (with-eval-after-load 'org-capture
    (defun ox-haunt-new-subtree-post-capture-template ()
      "Returns `org-capture' template string for new Haunt post.
See `org-capture-templates' for more information."
      (let* ((title (read-from-minibuffer "Post Title: ")) ;Prompt to enter the post title
             (file-name (string-join (split-string (downcase title)) "-")))
        (mapconcat #'identity
                   `(
                     ,(concat "* TODO " title)
                     ":PROPERTIES:"
                     ,(concat ":EXPORT_FILE_NAME: " file-name)
                     ":END:"
                     "%?\n")          ;Place the cursor here finally
                   "\n"))))

  (setq org-capture-templates
        `(("b"                ;`org-capture' binding + h
           "Blog post"
           entry
           ;; It is assumed that below file is present in `org-directory'
           ;; and that it has a "Blog Ideas" heading. It can even be a
           ;; symlink pointing to the actual location of all-posts.org!
           (file ,org-default-blog-file)
           (function ox-haunt-new-subtree-post-capture-template))
	  ("i" "Inbox" entry (file ,org-default-inbox-file)
           "* %? \n:PROPERTIES:\n:CREATED: %U\n:END:\n\n%x %i \n\nFrom: %a")
	  ("p" "Poem" entry
	   (file ,org-default-poems-file)
	   (function ox-haunt-new-subtree-post-capture-template))
	  ("w" "Website" entry (file ,org-default-inbox-file)
	   "* %a \n:PROPERTIES:\n:CREATED: %U\n:END:\n\n %?%:initial"
	   :immediate-finish t)))

  (setq org-mu4e-link-query-in-headers-mode nil)
  (setq org-catch-invisible-edits 'show-and-error)

  (global-set-key (kbd "C-c c") 'org-capture)

  (add-hook 'org-mode-hook 'org-indent-mode)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . T)
     (python . T)
     (ruby . T)
     (lisp . T)
     (js . T)
     (octave . T)
     (clojure . T)
     (dot . T)
     (R . T)
     (gnuplot . T))))

(use-package org-drill
  :ensure t
  :bind (:map org-mode-map
              ("C-c D" . org-drill)))

(use-package org-download
  :bind (:map org-mode-map
              ("C-c w" . org-download-screenshot)))

(use-package org-protocol
  :after org)

(use-package org-mu4e
  :after mu4e)

(use-package ox-haunt
  :after org)

(use-package org-noter
  :ensure t
  :bind (:map org-mode-map
              ("C-c n" . org-noter))
  :config
  (org-noter-set-auto-save-last-location t))

(use-package htmlize
  :ensure t
  :after org
  :config
  (setq org-html-htmlize-output-type 'css))

(use-package ox-reveal
  :after org
  :ensure t)

(use-package org-clock
  :after org)

;; org-bullets
(use-package org-bullets
  :ensure t
  :hook (org-mode . org-bullets-mode))

;; markdown
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)
         ("\\.markdown\\'" . gfm-mode))
  :init (setq markdown-command "multimarkdown")
  :config
  (defun my-markdown-mode-hook ()
    "Hook for markdown mode"
    (auto-fill-mode)
    (flyspell-mode))

  (add-hook 'markdown-mode-hook 'my-markdown-mode-hook))

;; erc
(use-package erc
  :commands (erc erc-tls)
  :bind
  ("C-c e b" .
   (lambda ()
     (interactive)
     (erc-tls :server "im.codemonkey.be" :port "6697" :nick "bendersteed")))
  ("C-c e e" .
   (lambda ()
     (interactive)
     (erc-tls :server "efnet.port80.se" :port "6697" :nick "bendersteed")))
  ("C-c e f" .
   (lambda ()
     (interactive)
     (erc-tls :server "irc.freenode.net" :port "6697" :nick "bendersteed")))
  ("C-c e l" .
   (lambda ()
     (interactive)
     (erc-tls :server "irc.lainchan.org" :port "6697" :nick "bendersteed")))
  ("C-c e g" .
   (lambda ()
     (interactive)
     (erc-tls :server "otrere.irc.gr" :port "9667" :nick "bendersteed")))
  ("C-c e c" .
   (lambda ()
     (interactive)
     (erc-tls :server "irc.cyberia.is" :port "6697" :nick "bd")))
  :custom
  (erc-autojoin-timing 'ident)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 22)
  (erc-prompt-for-nickserv-password nil)
  (erc-hide-list '("JOIN" "PART" "QUIT"))
  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 3)
  (erc-lurker-hide-list (quote ("JOIN" "PART" "QUIT")))
  (erc-lurker-threshold-time 43200)
  (erc-track-exclude-types '("JOIN" "MODE" "NICK" "PART" "QUIT"
                             "324" "329" "332" "333" "353" "477"))
  (erc-modules
   '(autoaway completion dcc services highlight-nicknames spelling notifications netsplit fill button match track readonly networks ring autojoin noncommands irccontrols move-to-prompt stamp menu list))
  :config
  (require 'erc-highlight-nicknames)
  (add-to-list 'erc-modules 'notifications)
  (add-to-list 'erc-modules 'spelling)
  (add-to-list 'erc-modules 'highlight-nicknames)
  (erc-services-mode 1)
  (erc-update-modules)
  :init
  (setq erc-autojoin-channels-alist '(("freenode.net" "#emacs" "#gnu" "#erc" "#lisp" "#guix" "##math" "#math-software" "##philosophy" "##statistics")))
  (setq erc-nick "bendersteed")
  ;; Rename server buffers to reflect the current network name instead
  ;; of SERVER:PORT (e.g., "freenode" instead of "irc.freenode.net:6667").
  ;; This is useful when using a bouncer like ZNC where you have multiple
  ;; connections to the same server.
  (setq erc-rename-buffers t)

  ;; Interpret mIRC-style color commands in IRC chats
  (setq erc-interpret-mirc-color t)

  ;; The following are commented out by default, but users of other
  ;; non-Emacs IRC clients might find them useful.
  ;; Kill buffers for channels after /part
  (setq erc-kill-buffer-on-part t)
  ;; Kill buffers for private queries after quitting the server
  (setq erc-kill-queries-on-quit t)
  ;; Kill buffers for server messages after quitting the server
  (setq erc-kill-server-buffer-on-quit t))

;; stackexchange
(use-package sx
  :ensure t
  :config
  (bind-keys :prefix "C-c x"
             :prefix-map my-sx-map
             :prefix-docstring "Global keymap for SX."
             ("q" . sx-tab-all-questions)
             ("i" . sx-inbox)
             ("o" . sx-open-link)
             ("u" . sx-tab-unanswered-my-tags)
             ("a" . sx-ask)
             ("s" . sx-search)))

;; elfeed
(use-package elfeed
  :ensure t
  :commands elfeed
  :init (setf url-queue-timeout 30)
  :config
  (setq-default elfeed-search-filter "@1-week-ago +unread")
  (push "-k" elfeed-curl-extra-arguments)

  (defun elfeed-play-with-mpv ()
    "Play entry link with mpv."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (quality-arg "")
          (quality-val (completing-read "Max height resolution (0 for unlimited): " '("0" "480" "720") nil nil)))
      (setq quality-val (string-to-number quality-val))
      (message "Opening %s with height≤%s with mpv..." (elfeed-entry-link entry) quality-val)
      (when (< 0 quality-val)
        (setq quality-arg (format "--ytdl-format=[height<=?%s]" quality-val)))
      (start-process "elfeed-mpv" nil "mpv" quality-arg (elfeed-entry-link entry))))

  (defvar elfeed-mpv-patterns
    '("youtu\\.?be")
    "List of regexp to match against elfeed entry link to know
whether to use mpv to visit the link.")

  (defun elfeed-visit-or-play-with-mpv ()
    "Play in mpv if entry link matches `elfeed-mpv-patterns', visit otherwise.
  See `elfeed-play-with-mpv'."
    (interactive)
    (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
          (patterns elfeed-mpv-patterns))
      (while (and patterns (not (string-match (car elfeed-mpv-patterns) (elfeed-entry-link entry))))
        (setq patterns (cdr patterns)))
      (if patterns
          (elfeed-play-with-mpv)
        (if (eq major-mode 'elfeed-search-mode)
            (elfeed-search-browse-url)
          (elfeed-show-visit)))))

  (defvar youtube-feed-format
    '(("^UC" . "https://www.youtube.com/feeds/videos.xml?channel_id=%s")
      ("^PL" . "https://www.youtube.com/feeds/videos.xml?playlist_id=%s")
      (""    . "https://www.youtube.com/feeds/videos.xml?user=%s")))

  (defun elfeed--expand (listing)
    "Expand feed URLs depending on their tags."
    (cl-destructuring-bind (url . tags) listing
      (cond
       ((member 'youtube tags)
        (let* ((case-fold-search nil)
               (test (lambda (s r) (string-match-p r s)))
               (format (cl-assoc url youtube-feed-format :test test)))
          (cons (format (cdr format) url) tags)))
       (listing))))

  (defmacro elfeed-config (&rest feeds)
    "Minimizes feed listing indentation without being weird about it."
    (declare (indent 0))
    `(setf elfeed-feeds (mapcar #'elfeed--expand ',feeds)))

  (elfeed-config
    ("https://solar.lowtechmagazine.com/feeds/all.rss.xml" eco tech)
    ("https://twobithistory.org/feed.xml" blog tech history)
    ("http://wingolog.org/feed/atom" blog tech guile js)
    ("https://datagubbe.se/atom.xml" blog tech)
    ("https://seirdy.one/posts/index.xml" blog tech)
    ("http://www.loper-os.org/?feed=rss2" blog tech)
    ("https://monosemena.gr/feed/" blog dwra)
    ("https://drewdevault.com/blog/index.xml" blog tech)
    ("https://cadelwatson.com/blog/rss/" blog tech linguistics)
    ("https://writing.kemitchell.com/feed.xml" foss tech law blog)
    ("https://nullprogram.com/feed/" blog tech emacs)
    ("https://sachachua.com/blog/feed/" emacs blog)
    ("https://feeds.feedburner.com/MostlyMaths" emacs math blog)
    ("https://oremacs.com/atom.xml" emacs blog)
    ("https://irreal.org/blog/?feed=rss2" blog emacs)
    ("https://blog.binchen.org/rss.xml" blog emacs)
    ("https://changelog.complete.org/feed" blog emacs)
    ("https://emacsair.me/feed.xml" blog emacs)
    ("https://www.hackaday.com/rss.xml" tech)
    ("https://arstechnica.com/feed/" tech)
    ("https://planet.gnu.org/rss20.xml" tech gnu planet)
    ("https://planet.emacslife.com/atom.xml" tech emacs planet)
    ("http://www.scheme.dk/planet/atom.xml" tech scheme planet)
    ("https://planet.lisp.org/rss20.xml" tech lisp)
    ("https://www.schneier.com/blog/atom.xml" crypto blog)
    ("https://esr.ibiblio.org/?feed=rss2" blog)
    ("https://sanctum.geek.nz/arabesque/feed/" blog tech)
    ("https://iridakos.com/feed.xml" blog tech)
    ("http://bit-player.org/feed" math blog)
    ("https://andregarzia.com/feeds/all.rss.xml" blog tech)
    ("https://www.phdcomics.com/gradfeed.php" comic)
    ("https://xkcd.com/rss.xml" comic)
    ("https://theoatmeal.com/feed/rss" comic)
    ("https://www.smbc-comics.com/rss.php" comic)
    ("https://www.commitstrip.com/en/feed/" comic)
    ("https://www.exocomics.com/feed" comic)
    ("http://www.eff.org/rss/updates.xml" tech)
    ("https://science.sciencemag.org/rss/twis.xml" science)
    ("https://www.gkbrk.com/feed.xml" blog tech)
    ("http://nullradix.eth.link/feed.xml" blog tech)
    ("http://jakob.space/feed.xml" blog emacs tech )
    ("https://updates.orgmode.org/feed/bugs" org emacs tech)
    ("https://alex-hhh.github.io/feeds/all.rss.xml" tech racket blog arduino)
    ("http://blog.benwiener.com/feed.xml" blog tech)
    ("http://www.xn--hrdin-gra.se/feed/all.rss.xml" blog tech demoscene)
    ("https://nyxus.xyz/posts/index.xml" blog an)
    ("http://verisimilitudes.net/rss.xml" blog tech lisp)
    ("https://ag91.github.io/rss.xml" blog tech)
    ("https://illusioncity.net/feed/" blog tech)
    ("https://mythoughtsbornfromfire.wordpress.com/feed/" blog)
    ("https://nora.codes/index.xml" blog tech)
    ("https://cdn.jwz.org/blog/feed/" blog tech news)
    ("https://bloghoskins.blogspot.com/feeds/posts/default" blog tech electronics)
    ("UCafxR2HWJRmMfSdyZXvZMTw" youtube)  ; Look MUM no computer
    ("1veritasium" youtube)             ; Veritasium
    ("UCYO_jab_esuFRV4b17AJtAw" youtube) ; 3Blue1Brown
    ("UCsXVk37bltHxD1rDPwtNM8Q" youtube) ; Kurzgesagt – In a Nutshell
    ("Wendoverproductions" youtube)
    ("UCZKQv0ZFHpeIUkOtNjtq4KA" youtube) ; Petscop
    ("UCvrLvII5oxSWEMEkszrxXEA" youtube) ; NODE
    ("UCUR1pFG_3XoZn3JNKjulqZg" youtube) ; thoughtbot
    ("UCDgRG-JR_ouT5-4wE13hbWA" youtube) ; abo-abo
    ("UCsgl7n_Zj35ODRZ_a_K5R-A" youtube) ; BuildFunThings
    ("UClcE-kVhqyiHCcjYwcpfj9w" youtube) ; LiveOverflow
    ("UC7pp40MU_6rLK5pvJYG3d0Q" youtube) ; Ethan & Hila
    ("UCtw5ExEjof2xQccg3mjzHNQ" youtube) ; Costantinov
    ("UCDWIvJwLJsE4LG1Atne2blQ" youtube) ; h3h3Productions
    ("UCxkMDXQ5qzYOgXPRnOBrp1w" youtube) ; Mike Zamansky
    ("UCoxcjq-8xIDTYp3uz647V5A" youtube) ; Numberphile
    ("UC1_uAIS3r8Vu6JjXWvastJg" youtube) ; MAthologer
    ("UC9-y-6csu5WGm29I7JiwpnA" youtube) ; Computerphile
    ("UCS0N5baNlQWJCUrhCEo8WlA" youtube) ; Ben Eater
    ("UCqgB5tox0TrGd26NH1wqTvg" youtube) ; Discretised
    ("Bisqwit" youtube)                 ; Bisqwit
    ("https://en.wikinews.org/w/index.php?title=Special:NewsFeed&feed=atom&categories=Published&notcategories=No%20publish%7CArchived%7CAutoArchived%7Cdisputed&namespace=0&count=30&hourcount=124&ordermethod=categoryadd&stablepages=only" news))

  (define-key elfeed-search-mode-map (kbd "b") 'elfeed-visit-or-play-with-mpv)
  (define-key elfeed-search-mode-map (kbd "u") 'elfeed-update)
  (define-key elfeed-show-mode-map (kbd "b") 'elfeed-visit-or-play-with-mpv))

;; (use-package exwm
;;   :ensure t
;;   :config
;;   (require 'exwm)

;;   ;; All buffers created in EXWM mode are named "*EXWM*". You may want to
;;   ;; change it in `exwm-update-class-hook' and `exwm-update-title-hook', which
;;   ;; are run when a new X window class name or title is available.  Here's
;;   ;; some advice on this topic:
;;   ;; + Always use `exwm-workspace-rename-buffer` to avoid naming conflict.
;;   ;; + For applications with multiple windows (e.g. GIMP), the class names of
;;                                         ;    all windows are probably the same.  Using window titles for them makes
;;   ;;   more sense.
;;   ;; In the following example, we use class names for all windows except for
;;   ;; Java applications and GIMP.
;;   (add-hook 'exwm-update-class-hook
;;             (lambda ()
;;               (unless (or (string-prefix-p "sun-awt-X11-" exwm-instance-name)
;;                           (string= "gimp" exwm-instance-name))
;;                 (exwm-workspace-rename-buffer exwm-class-name))))
;;   (add-hook 'exwm-update-title-hook
;;             (lambda ()
;;               (when (or (not exwm-instance-name)
;;                         (string-prefix-p "sun-awt-X11-" exwm-instance-name)
;;                         (string= "gimp" exwm-instance-name))
;;                 (exwm-workspace-rename-buffer exwm-title))))

;;   ;; To add a key binding only available in line-mode, simply define it in
;;   ;; `exwm-mode-map'.  The following example shortens 'C-c q' to 'C-q'.
;;   (define-key exwm-mode-map [?\C-q] #'exwm-input-send-next-key)

;;   ;; The following example demonstrates how to use simulation keys to mimic
;;   ;; the behavior of Emacs.  The value of `exwm-input-simulation-keys` is a
;;   ;; list of cons cells (SRC . DEST), where SRC is the key sequence you press
;;   ;; and DEST is what EXWM actually sends to application.  Note that both SRC
;;   ;; and DEST should be key sequences (vector or string).
;;   (setq exwm-input-simulation-keys
;;         '(
;;           ;; movement
;;           ([?\C-b] . [left])
;;           ([?\M-b] . [C-left])
;;           ([?\C-f] . [right])
;;           ([?\M-f] . [C-right])
;;           ([?\C-p] . [up])
;;           ([?\C-n] . [down])
;;           ([?\C-a] . [home])
;;           ([?\C-e] . [end])
;;           ([?\M-v] . [prior])
;;           ([?\C-v] . [next])
;;           ([?\C-d] . [delete])
;;           ([?\C-k] . [S-end delete])
;;           ;; cut/paste.
;;           ([?\C-w] . [?\C-x])
;;           ([?\M-w] . [?\C-c])
;;           ([?\C-y] . [?\C-v])
;;           ;; search
;;           ([?\C-s] . [?\C-f])))
  
;;   (setq exwm-input-global-keys
;;         `(([?\s-q] . (lambda () (interactive) (kill-buffer)))
;;           ([?\s-t] . display-time-mode)
;;           ([?\s--] . shrink-window)
;;           ([?\s-=] . enlarge-window)
;;           ([?\s-o] . other-window)
;;           ([?\s-s] . split-window-below)
;;           ([?\s-S] . split-window-right)
;;           ([?\s-b] . counsel-switch-buffer)
;;           ([?\s-0] . delete-window)
;;           ([?\s-1] . delete-other-windows)
;;           ([?\s-L] . (start-process "slock" nil "slock"))
;;           ([XF86Forward] . next-buffer)
;;           ([XF86Back] . previous-buffer)
;;           ([?\s-l] . next-buffer)
;;           ([?\s-h] . previous-buffer)
;;           ;; Bind "s-r" to exit char-mode and fullscreen mode.
;;           ([?\s-r] . exwm-reset)
;;           ([?\s-p] . password-store-copy)
;;           ;; Bind "s-w" to switch workspace interactively.
;;           ([?\s-w] . exwm-workspace-switch)
;;           ([?\s-y] . counsel-linux-app)))

;;   (require 'exwm-randr)
;;   (setq exwm-randr-workspace-output-plist '(0 "HDMI2"))
;;   (add-hook 'exwm-randr-screen-change-hook
;;             (lambda ()
;;               (start-process-shell-command
;;                "xrandr" nil "xrandr --output HDMI2 --left-of HDMI1 --auto")))
;;   (exwm-randr-enable)

;;   (exwm-enable))

;; end of init
(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(provide 'init)
;;; init.el ends here
