(defun pulse-line (&rest _)
  "Pulse the current line"
  (pulse-momentary-highlight-one-line (point)))

(advice-add #'other-window :after #'pulse-line)
