(defvar org-default-projects-dir   "~/notes/projects/"               "Project org files reside here")
(defvar org-default-lore-file      "~/notes/lore.org"               "Knowledge Base")
(defvar org-default-personal-file  "~/notes/personal.org"           "File of un-shareable, personal notes")
(defvar org-default-inbox-file     "~/notes/inbox.org"              "New stuff collects in this file")
(defvar org-default-tasks-file     "~/notes/tasks.org"              "Tasks, TODOs and little projects")
(defvar org-default-ideas-file     "~/notes/ideas.org"              "Collect dreams")
(defvar org-default-media-file     "~/notes/media.org"              "Reading, anime and movie lists, organised with tags")
(defvar org-default-blog-file      "~/notes/blog.org"               "My personal blog")
(defvar org-default-poems-file     "~/notes/poems.org"              "My poems")
(defvar org-default-projects-files
  (directory-files "~/notes/projects" :match ".org")
  "A list of all the project files")
(defvar org-journal-dir            "~/notes/journal/"                "The directory of journal files")
(defvar org-default-work-dir       "~/notes/work/"                   "Work related org files reside here")
(defvar org-default-work-files
  (directory-files "~/notes/work" :match ".org")
  "A list of all the work files")

(setq org-agenda-files '("~/notes/" "~/notes/projects/" "~/notes/work/"))

(setq org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil)

(setq org-refile-targets (append '((org-default-media-file :level . 1)
				   (org-default-personal-file :level . 0)
				   (org-default-lore-file :level . 0)
                                   (org-default-tasks-file :level . 0)
				   (org-default-ideas-file :level . 0)
				   (org-default-projects-files :level . 3)
				   (org-default-work-files :level . 0))))

(defun org-subtree-region ()
  "Return a list of the start and end of a subtree."
  (save-excursion
    (list (progn (org-back-to-heading) (point))
          (progn (org-end-of-subtree)  (point)))))

(defun dump-it (file contents)
  (find-file-other-window file-dest)
  (goto-char (point-max))
  (insert "\n" contents))

(defun org-refile-directly (file-dest)
  "Move the current subtree to the end of FILE-DEST.
If SHOW-AFTER is non-nil, show the destination window,
otherwise, this destination buffer is not shown."
  (interactive "fDestination: ")
  (save-excursion
    (let* ((region (org-subtree-region))
           (contents (buffer-substring (car region) (cadr region))))
      (apply 'kill-region region)
      (if org-refile-directly-show-after
          (save-current-buffer (dump-it file-dest contents))
        (save-window-excursion (dump-it file-dest contents))))))

(defvar org-refile-directly-show-after nil
  "When refiling directly (using the `org-refile-directly'
function), show the destination buffer afterwards if this is set
to `t', otherwise, just do everything in the background.")

(defun org-refile-to-ideas ()
  "Refile (move) the current Org subtree to `org-default-ideas-fire'."
  (interactive)
  (org-refile-directly org-default-ideas-file))

(defun org-refile-to-task (org-heading)
  "Refile (move) the current Org subtree to `org-default-tasks-file' and schedule it."
  (interactive "P")
  (org-todo)
  (org-schedule org-heading)
  (org-refile-directly org-default-tasks-file))

(defun org-refile-to-lore ()
  "Refile (move) the current Org subtree to `org-default-lore-file'."
  (interactive)
  (org-refile-directly org-default-lore-file))

(defun org-refile-to-personal ()
  "Refile (move) the current Org subtree to `org-default-personal-file'."
  (interactive)
  (org-refile-directly org-default-personal-file))

(defun remove-spaces-from-string (s)
  (coerce
   (loop for i across s
	 if (not (char-equal i ?\s))
	 collect i
	 else collect ?-)
   'string))

(defun org-refile-to-projects-dir ()
  "Refile the contents of the current Org subtree to a file in
`org-default-projects-files' in a file called like the subtree."
  (interactive)
  (let* ((heading (nth 4 (org-heading-components)))
	 (project-name (downcase (remove-spaces-from-string heading)))
	 (project-file (concat org-default-projects-dir project-name ".org")))
    (org-refile-directly project-file)))

;; hydra for routine
(global-set-key
 (kbd "C-c i")
 (defhydra hydra-org-refiler (:hint nil)
   ("k" org-previous-visible-heading "previous" :column "Navigate")
   ("j" org-next-visible-heading "next")
   ("c" org-archive-subtree-as-completed "archive")
   ("d" org-cut-subtree "delete")
   ("t" org-refile-to-task "tasks" :column "Refile")
   ("i" org-refile-to-ideas "ideas")
   ("p" org-refile-to-personal "personal")
   ("l" org-refile-to-lore "lore")
   ("r" org-refile "refile")
   ("m X" org-refile-to-projects-dir "projects" :column "Move")
   ("T" org-todo "todo" :column "Update")
   ("S" org-schedule "schedule")
   ("D" org-deadline "deadline")
   ("R" org-rename-header "rename")
   ("g t" (find-file-other-window org-default-tasks-file) "tasks" :column "Go to")
   ("g i" (find-file-other-window org-default-ideas-file) "ideas")
   ("g x" (find-file-other-window org-default-inbox-file) "inbox")
   ("g b" (find-file-other-window org-default-blog-file) "blog")
   ("g p" (find-file-other-window org-default-personal-file) "personal")
   ("g l" (find-file-other-window org-default-lore-file) "lore")
   ("g X" (dired org-default-projects-dir) "projects" :column "Dired")
   ("g C" (dired org-journal-dir) "journals")
   ("g W" (dired org-default-work-dir) "work")
   ("q" nil "quit" :column nil)))

(defun org-rename-header (label)
  "Rename the current section's header to LABEL, and moves the
point to the end of the line."
  (interactive (list
                (read-string "Header: "
                             (substring-no-properties (org-get-heading t t t t)))))
  (org-back-to-heading)
  (replace-string (org-get-heading t t t t) label))

;; journaling system
(defun org-archive-subtree-as-completed ()
  "Archives the current subtree to today's current journal entry."
  (interactive)
  ;; According to the docs for `org-archive-subtree', the state should be
  ;; automatically marked as DONE, but I don't notice that:
  (when (org-get-todo-state)
    (org-todo "DONE"))
  (let ((org-archive-location (format "%s::" (todays-journal-entry))))
    (org-archive-subtree)))

(defun todays-journal-entry ()
  "Return the full pathname to the day's journal entry file.
Granted, this assumes each journal's file entry to be formatted
with day/month/year, as in `04012019' for January 4th.

Note: `org-journal-dir' variable must be set to the directory
where all good journal entries live, e.g. ~/journal."
  (let* ((daily-name   (format-time-string "%d%m%Y.org"))
         (file-name    (concat org-journal-dir daily-name)))
    (expand-file-name file-name)))

(defun morning-routine ()
  "Load the default tasks file and start our hydra on the first task shown."
  (interactive)
  (let ((org-startup-folded nil))
    (find-file org-default-inbox-file)
    (delete-other-windows)
    (my-agenda)
    (pop-to-buffer (get-file-buffer org-default-inbox-file))
    (goto-char (point-min))
    (org-next-visible-heading 1)
    (hydra-org-refiler/body)))

(defun my-agenda ()
  "Display agenda"
  (interactive)
  (org-agenda nil "n")
  (get-buffer "*Org Agenda*")
  (execute-kbd-macro (kbd "lvd.")))
